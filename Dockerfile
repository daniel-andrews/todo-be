FROM ruby:2.6.3

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev

ENV HOME /rails
ENV RAILS_ENV development
ENV SECRET_KEY_BASE abcdefgh12345678

WORKDIR $HOME

# Install gems
COPY Gemfile* $HOME/
RUN gem update bundler
RUN bundle install

EXPOSE 3001

# Add the app code
COPY . $HOME

# Default command
CMD ["/bin/sh", "-c", "rails s puma -b 0.0.0.0 -p 3001"]
